#pragma once
#include <string>

template <typename T>
struct Node
{
	Node *pNext;
	Node *pPrevious;
	T data;
	Node(T data = T(), Node *pPrevious = nullptr, Node *pNext = nullptr)
	{
		this->data = data;
		this->pNext = pNext;
		this->pPrevious = pPrevious;
	}
};


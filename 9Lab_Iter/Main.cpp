#include "Human.h"
#include "List.h"

int main()
{
	while (true)
	{
		cout << "Choose a category to work with (1 - artist, 2 - programmer, 3 - gamedev, 0 - exit)" << endl;
		int numberOfCategory = check_range(cin, 0, 4);
		switch (numberOfCategory)
		{
		case 1:
		{
			{
				List<Artist> list;
				work_with_list(list);
			}
			break;
		}
		case 2:
		{
			{
				List<Programmer> list;
				work_with_list(list);
			}
			break;
		}
		case 3:
		{
			{
				List<Gamedev> list;
				work_with_list(list);
			}
			break;
		}
		case 0:
		{
			exit(0);
		}
		default:
			break;
		}
	}
	return 0;
}
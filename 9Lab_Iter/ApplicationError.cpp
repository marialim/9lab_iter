#include "ApplicationError.h"


InputError::InputError()
{
}

InputError::~InputError()
{
}

const char * InputError::what() const
{
	return "Input Error!!! Use only english letters!!! Please, try again.";
}


OutOfRangeError::OutOfRangeError()
{
}

OutOfRangeError::~OutOfRangeError()
{
}

const char * OutOfRangeError::what() const
{
	const char* str;
	str = new char[104];
	str = "Out_of_range Error!!! Please, try again";
	return str;
}


IsNotNumber::IsNotNumber()
{
}

IsNotNumber::~IsNotNumber()
{
}

const char * IsNotNumber::what() const
{
	return "Error!!! Please, enter a number";
}


string check_string(istream & in)
{
	string str;
	bool error;
	do {
		try {
			error = false;
			in >> str;
			for (int i = 0; i < str.length(); i++)
			{
				if (str[i] < 65 || str[i] > 122)
				{
					error = true;
					throw InputError();
				}
			}
		}
		catch (InputError& ex)
		{
			cout << ex.what() << endl;
		}
		catch (...)
		{
			cout << "Error" << endl;
		}
	} while (error);
	return str;
}

int check_range(istream & in, int range1, int range2)
{
	bool error;
	int year;
	do {
		try {
			error = false;
			in >> year;
			if (!in.good() || in.peek() != '\n')
			{
				throw IsNotNumber();
			}
			if (year > range2 || year < range1)
			{
				throw OutOfRangeError();
			}
		}
		catch (OutOfRangeError& ex)
		{
			error = true;
			in.clear();
			in.ignore(1000, '\n');
			cout << ex.what() << endl;
		}
		catch (IsNotNumber& ex)
		{
			error = true;
			in.clear();
			in.ignore(1000, '\n');
			cout << ex.what() << endl;
		}
	} while (error);
	return year;
}
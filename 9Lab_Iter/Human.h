#pragma once
#include "ApplicationError.h"
#include <iomanip>
#include "PC.h"

using namespace std;

class Human
{
protected:
	string name;
	string surname;
	int year;
public:
	Human(string n, string s, int y);
	Human();
	~Human();
	void setName(string n);
	void setSurname(string s);
	void setYear(int y);
	string getName();
	string getSurname();
	int getYear();
	friend ostream & operator<<(ostream & out, const Human & human);
	friend istream & operator>>(istream & in, Human & human);
	virtual void headOfTable();
	friend string check_string(istream & in);
	bool operator==(const Human& other);
};

class Artist : virtual public Human
{
protected:
	string paintingTechnique;
public:
	Artist(string n, string s, int y, string pT);
	Artist();
	~Artist();
	void setPaintingTechnique(string pT);
	string getPaintingTechnique();
	friend ostream & operator<<(ostream & out, const Artist & artist);
	friend istream & operator>>(istream & in, Artist & artist);
	void headOfTable() override;
};

class Programmer : virtual public Human
{
protected:
	string programmingLanguage;
	PC personalComp;
public:
	Programmer(string n, string s, int y, PC pC, string pL);
	Programmer();
	~Programmer();
	void setProgrammingLanguage(string pL);
	void setPersonalComp(PC pC);
	string getProgrammingLanguage();
	PC getPersonalComp();
	friend ostream & operator<<(ostream & out, const Programmer & programmer);
	friend istream & operator>>(istream & in, Programmer & programmer);
	void headOfTable() override;
};

ostream & operator<<(ostream & out, const Human & human);

ostream & operator<<(ostream & out, const Artist & artist);

ostream & operator<<(ostream & out, const Programmer & programmer);

istream & operator>>(istream & in, Human & human);

istream & operator>>(istream & in, Artist & artist);

istream & operator>>(istream & in, Programmer & programmer);

class Gamedev : public Artist, public Programmer
{
private:
	string platform;
	string graphics;
public:
	Gamedev(string n, string s, int y, string pT, PC pC, string pL, string p, string g);
	Gamedev();
	~Gamedev();
	void setPlatform(string p);
	void setGraphics(string g);
	string getPlatform();
	string getGraphics();
	friend ostream & operator<<(ostream & out, const Gamedev & gamedev);
	friend istream & operator>>(istream & in, Gamedev & gamedev);
	void headOfTable() override;
};

ostream & operator<<(ostream & out, const Gamedev & gamedev);

istream & operator>>(istream & in, Gamedev & gamedev);

#pragma once
#include <string>
#include <exception>
#include <iostream>

using namespace std;

class InputError : public std::exception
{
public:
	InputError();
	~InputError();
	const char* what() const override;
};

class OutOfRangeError : public std::exception
{
public:
	OutOfRangeError();
	~OutOfRangeError();
	inline const char* what() const override;
};

class IsNotNumber : public std::exception
{
public:
	IsNotNumber();
	~IsNotNumber();
	inline const char* what() const override;
};

string check_string(istream &in);
int check_range(istream &in, int range1, int range2);
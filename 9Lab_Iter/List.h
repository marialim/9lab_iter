#pragma once
#include <iostream>
#include "Node.h"
//#define n 2
#include "Human.h"

using namespace std;

template <class T>
class List
{
public:
	List();
	~List();
	class Iterator
	{
	public:
		Node<T>* current_value;
		Iterator(Node<T>* current);
		~Iterator();
		Iterator& operator=(const Iterator& other);
		Iterator& operator++();
		Iterator& operator--();
		Iterator& operator+(int i);
		Iterator& operator-(int i);
		Iterator& operator-(const Iterator& other);
		bool operator==(const Iterator& other);
		bool operator!=(const T& other);
		bool operator!=(const Iterator& other);
		T operator*();
	};
	void SortByYear(bool a_or_d(int a, int b));
	Iterator begin();
	Iterator end();
	void push_back(T data);
	int get_size();
	T operator[](const int index);
	void pop_back();
	void output();
private:
	int size;
	Node<T> *head;
	Node<T> *tail;
};

bool ascending(int a, int b)
{
	return(b < a);
}

bool descending(int a, int b)
{
	return (a < b);
}

template <class T>
List<T>::List()
{
	size = 0;
	head = nullptr;
	tail = nullptr;
}

template <class T>
List<T>::~List()
{
	Node<T>*  temp = head;
	while (temp)
	{
		Node<T>* temp2 = temp->pNext;
		delete  temp;
		temp = temp2;
	}
}

template<class T>
inline void List<T>::SortByYear(bool a_or_d(int a, int b))
{
	int a;
	for (Iterator i = begin(); i != end(); ++i)
	{
		for (Iterator j = i; j + 1 != end(); ++j)
		{

			bool b = (*a_or_d)(((*j).getYear()), ((*(j + 1)).getYear()));
			if (b)
			{
				swap(j, j + 1);
			}
		}
	}
}

template<class T>
typename List<T>::Iterator List<T>::begin()
{
	Iterator it(head);
	return it;
}

template<class T>
typename List<T>::Iterator List<T>::end()
{
	Iterator it(tail->pNext);
	return it;
}

template<class T>
void List<T>::push_back(T data)
{
	if (head == nullptr)
	{
		head = new Node<T>(data);
		tail = head;
	}
	else
	{
		Node<T> *current = this->head;
		while (current->pNext != nullptr)
		{
			current = current->pNext;
		}
		tail = new Node<T>(data, current);
		current->pNext = tail;
	}
	size++;
}

template<class T>
int List<T>::get_size()
{
	return size;
}

template<class T>
T List<T>::operator[](const int index)
{
	int counter = 0;
	Node<T> *current = this->head;
	while (current != nullptr)
	{
		if (counter == index)
		{
			return current->data;
		}
		current = current->pNext;
		counter++;
	}
}

template<class T>
void List<T>::pop_back()
{
	if (head == nullptr)
	{
		cout << "there is nothing to pop" << endl;
	}
	else
	{
		Node<T> *temp = tail;
		tail = tail->pPrevious;
		if (tail)
			tail->pNext = nullptr;
		size--;
	}
}

template<class T>
inline void List<T>::output()
{
	if (size == 0)
	{
		cout << "List is empty!" << endl;
		return;
	}
	T object;
	object.headOfTable();
	Node<T> *current = this->head;
	while (current != nullptr)
	{
		cout << current->data;
		current = current->pNext;
	}
}

template <class Cl>
void work_with_list(List <Cl>& list)
{
	while (true)
	{
		cout << "Enter a number of operation (1 - add element to list, 2 - get size of list, 3 -  find element in list, ";
		cout << "4 - delete last element, 5 - show list, 6 - return to category menu, 7 - sort list, 8 - search data in list, 0 - exit)" << endl;
		int numberOfOperation = check_range(cin, 0, 8);
		switch (numberOfOperation)
		{
		case 1:
		{
			cout << "Enter humans data:" << endl;
			Cl somebody;
			cin >> somebody;
			list.push_back(somebody);
		}
		break;
		case 2:
		{
			int size = list.get_size();
			cout << "Size: " << size << endl;
		}
		break;
		case 3:
		{
			int numberOfLastElement = list.get_size();
			if (!numberOfLastElement)
			{
				cout << "List is empty" << endl;
				break;
			}
			cout << "Enter a number of an element of list you want to get (indexing from 1)" << endl;
			int i = check_range(cin, 1, numberOfLastElement);
			Cl element = list[--i];
			element.headOfTable();
			cout << element << endl;
		}
		break;
		case 4:
		{
			list.pop_back();
		}
		break;
		case 5:
		{
			list.output();
		}
		break;
		case 6:
		{
			return;
		}
		break;
		case 7:
		{
			cout << "Choose: descending(1) or ascending(2):" << endl;
			int choice = check_range(cin, 1, 2);
			switch (choice)
			{
			case 1:
				list.SortByYear(descending);
				break;
			case 2:
				list.SortByYear(ascending);
				break;
			}
		}
		case 8:
		{
			//Search(list);
		}
		case 0:
		{
			exit(0);
		}
		default:
			cout << "Reenter please" << endl;
			break;
		}
	}
}

template<class T>
inline List<T>::Iterator::Iterator(Node<T>* curr) : current_value(curr)
{
}

template<class T>
inline List<T>::Iterator::~Iterator()
{
}

template<class T>
typename List<T>:: Iterator & List<T>::Iterator::operator=(const Iterator & other)
{
	if (this != &other)
	{
		current_value = other.current_value;
	}
	return *this;
}

template<class T>
typename List<T>:: Iterator & List<T>::Iterator::operator++()
{
	current_value = current_value->pNext;
	return *this;
}

template<class T>
typename List<T>::Iterator & List<T>::Iterator::operator--()
{
	current_value = current_value->pPrevious;
	return *this;
}

template<class T>
typename List<T>::Iterator & List<T>::Iterator::operator+(int i)
{
	Iterator iter(current_value);
	for (int j = 0; j < i; j++)
	{
		iter.current_value = iter.current_value->pNext;
	}
	return iter;
}

template<class T>
typename List<T>::Iterator & List<T>::Iterator::operator-(int i)
{
	for (int j = 0; j < i; j++)
	{
		current_value = current_value->pPrevious;
	}
	return *this;
}

template<class T>
typename List<T>::Iterator & List<T>::Iterator::operator-(const Iterator & other)
{
	for (int j = 0; j < other.current_value; j++)
	{
		current_value = current_value->pPrevious;
	}
	return current_value;
}

template<class T>
inline bool List<T>::Iterator::operator==(const Iterator& other)
{
	if (current_value == other->current_value) return true;
	else return false;
}

template<class T>
inline bool List<T>::Iterator::operator!=(const T & other)
{
	if (this->current_value->data == other) return false;
	else return true;
}

template<class T>
inline bool List<T>::Iterator::operator!=(const Iterator& other)
{
	if (this->current_value == other.current_value) return false;
	else return true;
}

template<class T>
typename T List<T>::Iterator::operator*()
{
	return current_value->data;
}

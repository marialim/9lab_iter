#include "Human.h"

Human::Human(string n, string s, int y) : name(n), surname(s), year(y)
{
}

Human::Human()
{
	name = "-";
	surname = "-";
	year = 1999;
}

Human::~Human()
{
}

void Human::setName(string n)
{
	name = n;
}

void Human::setSurname(string s)
{
	surname = s;
}

void Human::setYear(int y)
{
	year = y;
}

string Human::getName()
{
	return name;
}

string Human::getSurname()
{
	return surname;
}

int Human::getYear()
{
	return year;
}

void Human::headOfTable()
{
	cout << setw(15) << "Name" << setw(15) << "Surname" << setw(15) << "Year" << endl;
}

bool Human::operator==(const Human & other)
{
	if (name == other.name && surname == other.surname && year == other.year) return true;
	else return false;
}

Artist::Artist(string n, string s, int y, string pT) : paintingTechnique(pT), Human(n, s, y)
{
}

Artist::Artist() : Human()
{
	paintingTechnique = "-";
}

Artist::~Artist()
{
}

void Artist::setPaintingTechnique(string pT)
{
	paintingTechnique = pT;
}

string Artist::getPaintingTechnique()
{
	return paintingTechnique;
}

void Artist::headOfTable()
{
	cout << setw(15) << "Name" << setw(15) << "Surname" << setw(15) << "Year" << setw(15) << "Technique" << endl;;
}

Programmer::Programmer(string n, string s, int y, PC pC, string pL) : programmingLanguage(pL), Human(n, s, y)
{
	personalComp.company = pC.company;
	personalComp.operatingSystem = pC.operatingSystem;
}

Programmer::Programmer() : Human()
{
	programmingLanguage = "-";
	personalComp.company = "-";
	personalComp.operatingSystem = "-";
}

Programmer::~Programmer()
{
}

void Programmer::setProgrammingLanguage(string pL)
{
	programmingLanguage = pL;
}

void Programmer::setPersonalComp(PC pC)
{
	personalComp.company = pC.company;
	personalComp.operatingSystem = pC.operatingSystem;
}

string Programmer::getProgrammingLanguage()
{
	return programmingLanguage;
}

PC Programmer::getPersonalComp()
{
	return personalComp;
}

void Programmer::headOfTable()
{
	cout << setw(15) << "Name" << setw(15) << "Surname" << setw(15) << "Year" <<
		setw(15) << "Language" << setw(15) << "PC Company" << setw(15) << "PC OS" << endl;
}

ostream & operator<<(ostream & out, const Human & human)
{
	out << setw(15) << human.name << setw(15) << human.surname << setw(15) << human.year << endl;
	return out;
}

ostream & operator<<(ostream & out, const Artist & artist)
{
	out << setw(15) << artist.name << setw(15) << artist.surname << setw(15) << artist.year << setw(15)
		<< artist.paintingTechnique << endl;
	return out;
}

ostream & operator<<(ostream & out, const Programmer & programmer)
{
	out << setw(15) << programmer.name << setw(15) << programmer.surname << setw(15) <<
		programmer.year << setw(15) << programmer.programmingLanguage << setw(15) <<
		programmer.personalComp.company << setw(15) << programmer.personalComp.operatingSystem << endl;
	return out;
}

istream & operator>>(istream & in, Human & human)
{
	cout << "Enter human's name:" << endl;
	human.name = check_string(in);
	cout << "Enter human's surname:" << endl;
	human.surname = check_string(in);
	cout << "Enter human's birth year (1919-2019)" << endl;
	human.year = check_range(in, 1919, 2019);
	return in;
}

istream & operator>>(istream & in, Artist & artist)
{
	cout << "Enter name:" << endl;
	artist.name = check_string(in);
	cout << "Enter surname:" << endl;
	artist.surname = check_string(in);
	cout << "Enter birth year (1919-2019):" << endl;
	artist.year = check_range(in, 1919, 2019);
	cout << "Enter paintingTechnique:" << endl;
	artist.paintingTechnique = check_string(in);
	return in;
}

istream & operator>>(istream & in, Programmer & programmer)
{
	cout << "Enter name:" << endl;
	programmer.name = check_string(in);
	cout << "Enter surname:" << endl;
	programmer.surname = check_string(in);
	cout << "Enter birth year (1919-2019):" << endl;
	programmer.year = check_range(in, 1919, 2019);
	cout << "Enter language:" << endl;
	programmer.programmingLanguage = check_string(in);
	cout << "Enter PC data. Enter PC company:" << endl;
	programmer.personalComp.company = check_string(in);
	cout << "Enter PC operating system:" << endl;
	programmer.personalComp.operatingSystem = check_string(in);
	return in;
}

ostream & operator<<(ostream & out, const Gamedev & gamedev)
{
	out << setw(15) << gamedev.name << setw(15) << gamedev.surname << setw(15) <<
		gamedev.year << setw(15) << gamedev.paintingTechnique << setw(15) << gamedev.programmingLanguage << setw(15) << gamedev.personalComp.company
		<< setw(15) << gamedev.personalComp.operatingSystem << setw(15) <<
		gamedev.platform << setw(15) << gamedev.graphics << endl;
	return out;
}

istream & operator>>(istream & in, Gamedev & gamedev)//sostoianie potoka sho eto
{
	cout << "Enter name:" << endl;
	gamedev.name = check_string(in);
	cout << "Enter surname:" << endl;
	gamedev.surname = check_string(in);
	cout << "Enter birth year (1919-2019):" << endl;
	gamedev.year = check_range(in, 1919, 2019);
	cout << "Enter paintingTechnique:" << endl;
	gamedev.paintingTechnique = check_string(in);
	cout << "Enter language:" << endl;
	gamedev.programmingLanguage = check_string(in);
	cout << "Enter PC data. Enter PC company:" << endl;
	gamedev.personalComp.company = check_string(in);
	cout << "Enter PC operating system:" << endl;
	gamedev.personalComp.operatingSystem = check_string(in);
	cout << "Enter platform:" << endl;
	gamedev.platform = check_string(in);
	cout << "Enter graphics:" << endl;
	gamedev.graphics = check_string(in);
	return in;
}

Gamedev::Gamedev(string n, string s, int y, string pT, PC pC, string pL, string p, string g) : Artist(n, s, y, pT), Programmer(n, s, y, pC, pL), platform(p), graphics(g)
{
	name = n;
	surname = s;
	year = y;
}

Gamedev::Gamedev() : Artist(), Programmer()
{
	platform = "-";
	graphics = "-";
}

Gamedev::~Gamedev()
{
}

void Gamedev::setPlatform(string p)
{
	platform = p;
}

void Gamedev::setGraphics(string g)
{
	graphics = g;
}

string Gamedev::getPlatform()
{
	return platform;
}

string Gamedev::getGraphics()
{
	return graphics;
}

void Gamedev::headOfTable()
{
	cout << setw(15) << "Name" << setw(15) << "Surname" << setw(15) << "Year" << setw(15) << "Technique" <<
		setw(15) << "Language" << setw(15) << "PC Company" << setw(15) << "PC OS" <<
		setw(15) << "Platform" << setw(15) << "Graphics" << endl;
}